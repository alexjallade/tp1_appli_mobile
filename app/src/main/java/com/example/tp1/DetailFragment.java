package com.example.tp1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.tp1.data.Country;

import static com.example.tp1.data.Country.countries;

public class DetailFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        int i = args.getCountryId();
        TextView itemTitle = view.findViewById(R.id.textView);
        TextView itemCapital = view.findViewById(R.id.editTextTextPersonName2);
        TextView itemLangue = view.findViewById(R.id.editTextTextPersonName3);
        TextView itemMonnaie = view.findViewById(R.id.editTextTextPersonName5);
        TextView itemPopulation = view.findViewById(R.id.editTextTextPersonName4);
        TextView itemSuperficie = view.findViewById(R.id.editTextTextPersonName6);
        ImageView itemImage = view.findViewById(R.id.imageView2);

        itemTitle.setText(countries[i].getName());
        itemCapital.setText(countries[i].getCapital());
        itemLangue.setText(countries[i].getLanguage());
        itemMonnaie.setText(countries[i].getCurrency());
        itemPopulation.setText(String.valueOf(countries[i].getPopulation()));
        itemSuperficie.setText(String.valueOf(countries[i].getArea()));

        String uri = countries[i].getImgUri();
        //itemImage.setImage();
        Context c = itemImage.getContext();
        itemImage.setImageDrawable(c.getResources().getDrawable(
                c.getResources(). getIdentifier (uri , null , c.getPackageName())));

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}